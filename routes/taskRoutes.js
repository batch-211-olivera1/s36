const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskControllers");

// Routes ar responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used wen a route is accessed

// Route to get all the tasks
// this route expects to receive a GET request at the URL "/tasks"
// the whole URL is at "http://localhost:3001/tasks"
router.get("/", (req, res) =>{
	taskController.getAllTasks().then(resultFromController=>res.send(
		resultFromController));
})

// "module.exports" used to export the router object to use in the index.js
module.exports = router;


router.post("/", (req, res) => {
	taskController.createTasks(req.body).then(resultFromController=> res.send(resultFromController));
})

router.delete("/:id",(req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController));
})


router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})


//use "module.exports" to export the router object to use in the index.js
module.exports = router;


// Activity


router.get("/:id", (req, res) =>{
	taskController.getTask(req.params.id,req.body).then(resultFromController=>res.send(
		resultFromController));
})

router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})

// Separation of concerns
/*
	Why do we have to
	-Better code readability
	-Improve scalability
	-Better code maintainability
	What do we separare
	-Models - Contains what objects are needed in our API
		e.g users, course
	-Controllers - Contains instructions on how your API performe intended tasks
	-Routes - defines when 
*/

// Setup the dependencies/imports
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes")

// server setup

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// Connecting to mongoDB atlas


mongoose.connect("mongodb+srv://admin:admin123@project0.mwq6bdq.mongodb.net/?retryWrites=true&w=majority",
	{
	useNewUrlParser:true,
	useUnifiedTopology: true

	}
);

let db = mongoose.connection
db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));


app.use("/tasks",taskRoutes);

app.listen(port, () => console.log(`Server running at port ${port}`));